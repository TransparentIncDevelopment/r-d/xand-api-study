# XAND API Design

This project represents effort for a proposed user-centric REST XAND API spec for v1 requirements.

## Project License

MIT OR Apache-2.0

## V1 Requirements:

Public XAND API shape for v1 needs to support: 

- Redeem-only wallets
- Governance functionality (Proposing + Voting)
- Balance Retrieval
- Service Health Checks

## Guidelines

First, here is a reference to the TPFS Engineering guide: https://gitlab.com/TransparentIncDevelopment/docs/engineering-guide

We should keep principles from this guide in mind when maintaining this project.

This design effort will implement best-practice REST strategies to supply users with a predictable API using standard conventions that will be easy to adapt.  Our product has pivoted to these APIs themselves, and so the shapes of them require great care.  It can be expanded in the future to include data streams and other features, but the initial shape should be the minimum resource management and querying necessary to enable a viable dev environment.  With that in mind, the following are guidelines to adhere to when designing this specification:

### REST API Best Practices

[Microsoft REST API Design Best Practices](https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design)

Microsoft has an excellent article on REST API design linked above, and it is recommended reading for contributing to this API Spec.  In the article, there are various levels of strictness for a REST API's design.  Our goal is to adhere to at least level 2:

    Level 0: Define one URI, and all operations are POST requests to this URI.
    Level 1: Create separate URIs for individual resources.
    Level 2: Use HTTP methods to define operations on resources.
    Level 3: Use hypermedia (HATEOAS, described below).
    Level 3 corresponds to a truly RESTful API according to Fielding's definition. In practice, many published web APIs fall somewhere around level 2.

This means that all routes for the public XAND API should be resource-based in design and accessible via the common HTTP verbs as appropriate.

A full list of the common request verbs and their uses is available [here via Mozilla](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods) and described by [Microsoft's article linked above at this anchor](https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design#define-operations-in-terms-of-http-methods).

There is a more in-depth best practices guide available [here](https://github.com/Microsoft/api-guidelines/blob/master/Guidelines.md) from Microsoft as well.

### REST APIs Manage Resources

[Organize the API Around Resources](https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design#organize-the-api-around-resources)

A key takeaway from the Microsoft best practices article is the fact that REST APIs are designed to interact with the resources that our software products are built from:

    * REST APIs are designed around resources, which are any kind of object, data, or service that can be accessed by the client.

    * A resource has an identifier, which is a URI that uniquely identifies that resource.

    * Clients interact with a service by exchanging representations of resources. Many web APIs use JSON as the exchange format. 

It is important to remember that our designs will stem from a resource-based approach.  We will separate out our routes per-resource and in doing so will be able to define a high-level shape of the XAND ecosystem for any user that wants to build on it.

Users that build internal software using our APIs will need to know what resources they have access to and in what ways they can interact with those resources.  They use these resources in their own ways to create whatever tools they need, and so we should anticipate what those resources will be and provide them plainly.  This is in contrast to an approach where you would consider software use cases and create routes which support those actions directly almost like a CLI (via some `governance/xxx` or generic `propose` endpoint). 

### Language-Agnostic Specification

By using OpenAPI to define a specification for our XAND API, users gain the benefit of being able to use these documents to generate server, client, and data model code for their own uses.  Since we should use the same development processes that we will ask our API users to go through, we should treat this specification not only as a design document for the shape of our API but also as the source for any client + server implementation we may provide to users.  Users should be able to build client code from the specification document and interact with an already-running implementation of the XAND API server.  They should be able to do this in common languages like C#, Java, Rust, and Typescript.

### Data Structure Source of Truth

Because we should use the same process our users will use when developing software that uses the XAND API, we should treat the specification document as the source of truth for any model definitions.  Implementations in specific languages, even internally (i.e. Rust) should directly adapt from the generated code available to the users via this specification document, even if some transformations are necessary for the ecosystem being created.

"What is a Trust"
"What is a Proposal"
"What is an Address"

These are questions that should be immediately apparent by the definitions of structures in the specification.

### User-Friendly Documentation

All of our resources and their supported REST verbs should be easily visible and explained.  This is made easy with Swagger UI, but it still requires care to be taken when developing the specifications.  This means that there should be, at a bare minimum for all routes and models:

- A name
- A user-friendly description that uses proper domain language, grammar, and punctuation
- Correctness in all Required / Optional fields

In addition, it will be helpful to any user if we also take the time to look for other visualization tools for OpenAPI documents in case the Swagger UI can be augmented in some way.

We should also track changesets for the specification visibly.

## API Design Vision

```mermaid
graph LR
  RESOURCES{XAND Resources}

  RESOURCES -->API{XAND API}
  API -->  API_HEALTH(Services Health)
  API_HEALTH --> API_HEALTH_GET[GET]
  API --> API_SPECS(XAND API OpenAPI YAML)
  API_SPECS --> API_SPECS_GET[GET]
  API --> API_METADATA("Blockchain Metadata from Xandstrate (included in all GETs)")

  RESOURCES --> VALIDATORS{Validators}
  VALIDATORS --> VALIDATORS_GET[GET]
  VALIDATORS --> VALIDATORS_PROPOSALS{"Proposals"}
  VALIDATORS_PROPOSALS --> VALIDATORS_PROPOSALS_GET[GET]
  VALIDATORS_PROPOSALS --> VALIDATORS_ADD("Add")
  VALIDATORS_ADD --> VALIDATORS_ADD_GET[GET]
  VALIDATORS_ADD --> VALIDATORS_ADD_POST[POST]
  VALIDATORS_PROPOSALS --> VALIDATORS_REMOVE("Remove")
  VALIDATORS_REMOVE --> VALIDATORS_REMOVE_GET[GET]
  VALIDATORS_REMOVE --> VALIDATORS_REMOVE_POST[POST]

  RESOURCES --> MEMBERS{Members}
  MEMBERS --> MEMBERS_GET[GET]
  MEMBERS --> MEMBERS_PROPOSALS{"Proposals"}
  MEMBERS_PROPOSALS --> MEMBERS_PROPOSALS_GET[GET]
  MEMBERS_PROPOSALS --> MEMBERS_ADD("Add")
  MEMBERS_ADD --> MEMBERS_ADD_GET[GET]
  MEMBERS_ADD --> MEMBERS_ADD_POST[POST]
  MEMBERS_PROPOSALS --> MEMBERS_REMOVE("Remove")
  MEMBERS_REMOVE --> MEMBERS_REMOVE_GET[GET]
  MEMBERS_REMOVE --> MEMBERS_REMOVE_POST[POST]

  RESOURCES --> TRUST{Trust}
  TRUST --> TRUST_GET[GET]
  TRUST --> TRUST_PROPOSALS{Proposals}
  TRUST_PROPOSALS --> TRUST_PROPOSALS_GETS[GET]
  TRUST_PROPOSALS --> TRUST_SET(Set)
  TRUST_SET --> TRUST_SET_GET[GET]
  TRUST_SET --> TRUST_SET_POST[POST]

  RESOURCES --> CIDR(CIDR Blocks)
  CIDR --> CIDR_GET[GET]
  CIDR --> CIDR_POST[POST]
  CIDR --> CIDR_PUT[PUT]
  CIDR --> CIDR_DELETE[DELETE]

  RESOURCES --> VOTES(Votes)
  VOTES --> VOTES_GET[GET]
  VOTES --> VOTES_POST[POST]

  RESOURCES --> PROPOSALS(Proposals)
  PROPOSALS --> PROPOSALS_GET[GET]

  RESOURCES --> CLAIMS{Claims}
  CLAIMS --> CLAIMS_GET[GET]
  CLAIMS --> CLAIMS_REDEEM("Redemptions")
  CLAIMS_REDEEM --> CLAIMS_REDEEM_GET[GET]
  CLAIMS_REDEEM --> CLAIMS_REDEEM_POST[POST]
  CLAIMS --> CLAIMS_CREATE("Creations*")
  CLAIMS_CREATE --> CLAIMS_CREATE_GET[GET]
  CLAIMS_CREATE --> CLAIMS_CREATE_POST[POST]
  CLAIMS --> CLAIMS_XFER("Transfers*")
  CLAIMS_XFER --> CLAIMS_XFER_GET[GET]
  CLAIMS_XFER --> CLAIMS_XFER_POST[POST]
```

`*Claims Creation / Transfer not necessary for Redeem-only implementations`
